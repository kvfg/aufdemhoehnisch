# MRBS für ADH

Hintergrund: Eine weitere Instanz von MRBS war so einzurichten, dass dieses a) gegenüber Schulfremden verschlossen ist b) Lehrer:innen aber gucken dürfen c) das Sekretariat buchen kann und es d) trotzdem noch einen gibt, der davon unabhängig Adminrechte hat. MRBS bringt aber nur die Nutzerlevel 0-2 mit. Doof. Also musste ein weiterer Benutzerlevel her. 

1. Benutzer Lehrer:innen mit "Nur mal gucken"-Rechten
2. Benutzer Sekretariat mit Buchungsrechten
3. Benutzer Admin mit administrativen Rechten

## config.inc.php:

Erst mal MRBS gegenüber der Öffentlichkeit abschließen: 

````
// If you want to prevent the public (ie un-logged in users) from
// being able to view bookings completely, set this variable to true
$auth['deny_public_access'] = true;
````

Dann die Benutzerlevel frisch setzen:

````

// User levels
$max_level = 3;
$min_user_editing_level = 3;
$min_user_viewing_level = 3;
````

Wir überschreiben am Dateiende noch die Ausgabe von MRBS aus der Sprachdatei, damit wir "unsere" Worte wiederfinden.

## mrbs_auth.inc

Diese ist die entscheidende Datei. 

Hier wird zwischen den Zeilen 103 und 165 der Zugriff der einzelnen Benutzer:innen genauer definiert - und dann in Zeile 437 der administrative level auf 3 gesetzt. Unterbleibt der letzte Schritt, dann können alle Benutzer:innen die Buchungen der jeweils anderen Benutzer:innen überschreiben / ändern / löschen. Will man eher nicht - wegen Konfliktminimierung und so. 
